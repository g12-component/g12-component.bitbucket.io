
// * Scripts for {Project name and url}
// *
// * author: {name}
// * email: {email}
// * website: {portfolio}


const hamburger = document.querySelector(".hamburger");
const navLinks = document.querySelector(".nav-links");
const links = document.querySelectorAll(".nav-links li");

hamburger.addEventListener('click', ()=>{
   //Animate Links
    navLinks.classList.toggle("open");
    links.forEach(link => {
        link.classList.toggle("fade");
    });

    //Hamburger Animation
    hamburger.classList.toggle("toggle");
});


// sliding show js 

const slideshowImages = document.querySelectorAll(".intro-slideshow img");

const nextImageDelay = 5000;
let currentImageCounter = 0; // setting a variable to keep track of the current image (slide)

// slideshowImages[currentImageCounter].style.display = "block";
slideshowImages[currentImageCounter].style.opacity = 1;

setInterval(nextImage, nextImageDelay);

function nextImage() {
  // slideshowImages[currentImageCounter].style.display = "none";
  slideshowImages[currentImageCounter].style.opacity = 0;

  currentImageCounter = (currentImageCounter+1) % slideshowImages.length;

  // slideshowImages[currentImageCounter].style.display = "block";
  slideshowImages[currentImageCounter].style.opacity = 1;
}

// cards js start

function toggle() {
     var x = document.getElementById("texty");

     if(x.style.display === "none") {
          x.style.display = "block";
     }

     else {
          x.style.display = "none";
     }
}


// contact form message prompt //
function showAlert() {
     let e=document.getElementById("phoneNumber")
     if (e.value.length!=13)
     {
          alert("Please enter the 10 digit number pattern as 12-1-1234-123  ")
     }
     else{
          var myText = "Thanks for your feedback!";
          alert (myText);
     }

   }
