$(function(){
  
  //Clicking on the Save button will activate Successful Save message
  $(".saveButton").click(function(){
    $(".successfully-saved").css("display", "block").delay(1000).fadeOut(400);
  });
  
});